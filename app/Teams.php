<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Teams;
class Teams extends Model
{
    protected $fillable = ['image','team_name','description'];
    public function deleteImage(){
        Storage::delete($this->image);
    }
    // public function deleteImage(Teams $team){
       
    //     Storage::forceDelete($team->image);

    // }
}
