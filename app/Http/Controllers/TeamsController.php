<?php

namespace App\Http\Controllers;

use App\Teams;
use Illuminate\Http\Request;
use App\Http\Requests\Teams\CreateTeamRequest;
use App\Http\Requests\Teams\UpdateTeamRequest;
use Illuminate\Support\Facades\Storage;
class TeamsController extends Controller
{
    
    public function index()
    {
        //
        $teams = Teams::all();
        return view('teams.index', compact([
            'teams'
        ]));
    }

   
    public function create()
    {
        return view('teams.create');
        //
    }

    public function store(CreateTeamRequest $request)
    {
        //
        // $image = $request->file('image')->store('teams');
        $image = Storage::disk('public')->put('teams', $request->file('image'));

        Teams::create([
            'image'=>$image,
            'team_name'=>$request->team_name,
            'description'=>$request->description
        ]);
        

        session()->flash('success','Team Added Successfuly');
        return redirect(route('teams.index'));
    }

    public function show(Teams $team)
    {
        //
    }

    public function edit(Teams $team)
    {
        return view('teams.edit',compact(
            'team'
        ));
    }
    public function update(UpdateTeamRequest $request, Teams $team)
    {
         //
         $data = $request->only(['team_name' , 'description']);
         if($request->hasFile('image')){
            $team->deleteImage($team);
            session()->flash('success','Team  image deleted  successfuly!');
            $image = Storage::disk('public')->put('teams', $request->file('image'));
            $data['image']=$image;
         }
         $team->update($data);
         session()->flash('success','Team   updated successfuly!');
         return redirect(route('teams.index'));  
    }

    public function destroy(Teams $team)
    {
        //
        $team->deleteImage($team);
        $team->forceDelete();
        session()->flash('success','Team   Deleted  successfuly!');
         return redirect(route('teams.index'));  
    }
    // public function images(Teams $team ){
    //     return Teams::make(storage_path('public/'))->response();
    // }
}
