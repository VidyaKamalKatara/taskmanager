@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">Add Teams</div>

    <div class="card-body">
        <form action="{{ route('teams.store')}}" method="POST" enctype="multipart/form-data">
        @csrf 
        <div class="form-group">
        <label for="team_name"> Team Name </label>
        <input type="text"
            value="{{ old('team_name')}}"
            class="form-control @error('team_name') is-invalid @enderror"
            name="team_name" id="team_name">
            @error('team_name')
                <p class="text-danger"> {{ $message}}</p>
            @enderror
        </div>

        <div class="form-group">
                    <label for="description">Description</label>
                    <input id="description"s  type="hidden" name="description">
                <trix-editor input="description"></trix-editor>
                    <textarea name="description"
                              id="description"
                              class="form-control @error('description') is-invalid @enderror"
                              rows="4">{{old('description')}}</textarea>

                    @error('description')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
        </div>

        <div class="form-group">
            <label for="image">Image</label>
                <input type="file"
                       value="{{ old('image') }}"
                       class="form-control @error('image') is-invalid @enderror"
                       name="image" id="image">
                       @error('image')
                            <p class="text-danger">{{ $message }}</p>
                       @enderror
        </div>

        <div class="form-group">
        <button class="btn btn-success" type="submit">Add Team</button></div>
        
        </form>
    </div>


</div>
@endsection

@section('page-level-scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.3/trix.js"></script>
@endsection

@section('page-level-styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.3/trix.min.css">
@endsection