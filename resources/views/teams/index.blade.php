@extends('layouts.app')

@section('content')

<div class="d-flex justify-content-end mb-3 mr-4">
    <a href="{{ route('teams.create')}}" class="btn btn-primary"> Add Team</a>
</div>
<div class="card">
    <div class="card-header">TEAMS  </div>
</div>

<div class="card-body">
    <table class="table table-bordered">
        <thead>
            <th>Team Image</th>
            <th>Team Name</th>
            <th>Team Description</th>
            <th>Actions</th>
        </thead>
        <tbody>
            @foreach($teams as $team)
            <tr>
            <td><img src="{{asset('storage/'.$team->image)}}" alt="Team Image"  width="130"></td>
           
                <td>  {{$team->team_name}} </td>
                <td>  {{$team->description}}  </td>
                <td>
                <a href="{{ route('teams.edit', $team->id)}}" class="btn btn-primary btn-sm"> Edit</a>
                <a href="#" class="btn btn-danger btn-sm"
                onclick="displayModalForm({{$team}})"
                data-toggle="modal"
                data-target="#deleteModal">Delete</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
<!-- DELETE MODAL  -->

<!--DELETE MODAL-->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete Team</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="POST" id="deleteForm">
                    @csrf
                    @method('DELETE')
                    <div class="modal-body">
                        <p>Are you sure want to delete Team ??</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Delete Team</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--END DELETE MODAL-->
@endsection

@section('page-level-scripts')
    <script type="text/javascript">
        function displayModalForm($team) {
            var url = '/teams/' + $team.id;
            $("#deleteForm").attr('action', url);
        }
    </script>
@endsection








