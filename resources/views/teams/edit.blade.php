@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header"> Edit Team</div>

    <div class="card-body">
        <form action="{{ route('teams.update',$team->id)}}" method="POST" enctype="multipart/form-data">
        @csrf 
        @method('PUT')
        <div class="form-group">
        <label for="team_name"> Name</label>
        <input type="text"
            value="{{ old('team_name', $team->team_name) }}"
            class="form-control @error('team_name') is-invalid @enderror"
            name="team_name" id="team_name">
            @error('team_name')
                <p class="text-danger"> {{ $message}}</p>
            @enderror
        </div>

        <div class="form-group">
                    <label for="description">Description</label>
                    <input id="description" type="hidden" name="description" value="{{ old('description', $team->description) }}">
                    <trix-editor input="description"></trix-editor>
                    @error('description')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                
                <div class="form-group">
                    <img src="{{ asset('storage/'.$team->image) }}" alt="" width="auto">
                </div>

        <div class="form-group">
                    <label for="image">Image</label>
                    <input type="file"
                           class="form-control @error('image') is-invalid @enderror"
                           name="image" id="image">
                    @error('image')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>

        <div class="form-group">
        <button class="btn btn-warning" type="submit">Update Team </button></div>
        
        </form>
    </div>


</div>
@endsection

@section('page-level-scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.3/trix.js"></script>
@endsection

@section('page-level-styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.3/trix.min.css">
@endsection